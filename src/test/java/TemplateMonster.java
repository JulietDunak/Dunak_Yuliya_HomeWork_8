import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class TemplateMonster {
    WebDriver driver;
    @BeforeClass
    void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Yuliya\\Desktop\\setup\\chromedriver.exe");
        //System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
        driver = (WebDriver) new ChromeDriver();
        // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    void waitCookies (){
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until((WebDriver web) -> driver.manage().getCookieNamed("aff")!=null);
    }
    @Test
    void serchCookies (){
        waitCookies();
        Cookie aff = driver.manage().getCookieNamed("aff");
        Assert.assertEquals(aff.getValue(),"TM");
    }

    void selectLanguage (String language) {
        driver.findElement(By.id("menu-"+language+"-locale")).click();
    }
    @Test
    void testSelectLanguage (){
        driver.findElement(By.className("menu-element-inner")).click();
        selectLanguage("PL");
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.templatemonster.com/pl/");
    }
    @org.testng.annotations.AfterClass
    public void tearDown() {
        driver.quit();
    }
}
